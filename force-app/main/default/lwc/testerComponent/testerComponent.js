import { LightningElement } from 'lwc';

export default class TesterComponent extends LightningElement {

    showFullModal = false;
    showHeadlessModal = false;
    showFootlessModal = false;
    showBodyMarkupModal = false;

    // Handlers
    handleShowModal(event) {
        console.log('TesterComponent :: handleShowModal');
        // console.log(event.currentTarget.name);
        switch(event.currentTarget.name) {
            case 'fullModal':
                this.showFullModal = true;
                break;
            case 'headlessModal':
                this.showHeadlessModal = true;
                break;
            case 'footlessModal':
                this.showFootlessModal = true;
                break;
            case 'bodyMarkupModal':
                this.showBodyMarkupModal = true;
                break;
        }
    }

    handleCloseModal(event) {
        console.log('TesterComponent :: handleCloseModal');
        // console.log(event.detail);
        switch(event.detail) {
            case 'fullModal':
                this.showFullModal = false;
                break;
            case 'headlessModal':
                this.showHeadlessModal = false;
                break;
            case 'footlessModal':
                this.showFootlessModal = false;
                break;
            case 'bodyMarkupModal':
                this.showBodyMarkupModal = false;
                break;
        }
    }
}