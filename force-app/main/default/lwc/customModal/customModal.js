import { api, LightningElement } from 'lwc';

export default class CustomModal extends LightningElement {

    @api modalName;
    @api body;
    @api title;
    @api tagline;
    @api closeText;
    @api hideFooter;
    @api hideBackdrop;
    @api modalSize;

    get headerClass() {
        let baseClass = 'slds-modal__header';
        if(this.title === undefined && this.tagline === undefined) {
            return baseClass + ' slds-modal__header_empty';
        }
        return baseClass;
    }

    get closeLabel() {
        return this.closeText ? this.closeText : 'Close';
    }

    get modalClass() {
        console.log('get modalClass');
        let baseClass = 'slds-modal slds-fade-in-open';
        switch(this.modalSize) {
            case 'small':
                return baseClass += ' slds-modal_small';
            case 'large':
                return baseClass += ' slds-modal_large';
            default:
                return baseClass += ' slds-modal_medium'; 
        }
    }

    // Handlers
    handleClose(event) {
        console.log('CustomModal :: handleClose');
        this.dispatchEvent(new CustomEvent('closemodal', {detail: this.modalName}));
    }
}