# Dynamic Lightning Modal

## Overview

A Lightning Web Component that displays a pop-up modal with a number of customizable attributes.

## Attributes

| Name          | Description                                                                       |
|---------------|-----------------------------------------------------------------------------------|
| body          | Text that will display in the body of the modal                                   |
| title         | Text that will display in the title of the modal                                  |
| tagline       | Text that will display under the title of the modal                               |
| modal-size    | Size of the displayed modal. **Options:** "small", "medium", "large" **Default:** "medium" |
| hide-footer   | Hides modal footer if set to true **Default:** false                                  |
| hide-backdrop | Hides dark backdrop if set to true **Default:** false                                 |
| close-text    | Text label for the button that closes the modal. **Default:** "Close"                 |
| modal-name    | Value that is returned in event.detail in the event "onclosemodal"                 |

## Slots

| Name | Description                                                       |
|------|-------------------------------------------------------------------|
| body | HTML markup that will be rendered in the body of the modal if passed |

## Examples

### Full Modal (large)

![Full modal](media/full-modal.png)

```html
<c-custom-modal
    modal-name="fullModal"
    body="This is the body text for a full modal, which includes a title and tagline. It also contains a customized label for the close button."
    title="Large Full Modal"
    tagline="This is a tagline that goes beneath the title"
    close-text="Okay"
    modal-size="large"
    onclosemodal={handleCloseModal}>
</c-custom-modal>
```

### Modal With No Header (medium)

![Modal With No Header](media/headless-modal.png)

```html
<c-custom-modal
    modal-name="headlessModal"
    body="This is a headless modal that does not have a title or a tagline. It also has the default close button label."
    onclosemodal={handleCloseModal}>
</c-custom-modal>
```

### Modal With No Footer/Backdrop (small)

![Modal With No Footer/Backdrop](media/footless-modal.png)

```html
<c-custom-modal
    modal-name="footlessModal"
    body="This is the body of a modal where we hide the footer. We also hide the dark backdrop."
    title="Small, Footless Modal With No Backdrop"
    hide-footer=true
    hide-backdrop=true
    modal-size="small"
    onclosemodal={handleCloseModal}>
</c-custom-modal>
```

### Modal With Passed HTML Body (medium)

![Full modal](media/body-slot-modal.png)

```html
<c-custom-modal
    modal-name="bodyMarkupModal"
    title="Modal Using Body Slot"
    onclosemodal={handleCloseModal}>
    <span slot="body">
        <b>We can also pass a body through </b><i>the slot if we need to include </i><a href="javascript:void(0);">more complex markup!</a>
    </span>
</c-custom-modal>
```